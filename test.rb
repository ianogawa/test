def init
  array_content = []
  hash_content = Hash.new(0)
  path = ''

  puts "Input path ?"
  path = gets.chomp

  result = get_folder(array_content, hash_content, path)
  result_hash = assign_to_hash(result, hash_content)
  higher_value = Hash[result_hash.select {|k, v| v == result_hash.values.max}]

  higher_value.each do |k, v|
    puts "#{k} appears #{v} times"
  end
end

def get_folder(array_content, hash_content, path_input)
  dir_path = path_input == '' ? Dir.pwd : path_input

  Dir.foreach(dir_path) do |item|
    next if item == '.' || item == ".."

    path = File.realdirpath(item)
    is_file = File.file?(dir_path + item)

    if is_file
      File.foreach(dir_path + item).map { |line| array_content << line}
    else
      is_directory = File.directory?(dir_path + item)
      get_folder(array_content, hash_content, dir_path + item + '/') if is_directory
    end
  end
  return array_content
end

def assign_to_hash(result, hash_content)
  result.each do |value|
    hash_content[value] += 1
  end

  return hash_content
end

init